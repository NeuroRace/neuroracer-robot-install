#!/bin/bash

# get git directory path
git_path="`dirname \"$0\"`"              # relative
git_path="`( cd \"$git_path\" && pwd )`"  # absolutized and normalized
if [ -z "$git_path" ] ; then
  # error; for some reason, the path is not accessible
  # to the script (e.g. permissions re-evaled after suid)
  exit 1  # fail
fi

# enable universe, multiverse and restricted repo
sudo apt-add-repository multiverse
sudo apt-add-repository universe
sudo apt-add-repository restricted
sudo apt update

# install gstreamer plugin bad and ugly
sudo apt -y install gstreamer1.0-plugins-bad
sudo apt -y install gstreamer1.0-plugins-ugly

## NOTE:
## The provided opencv package by JetPack Installer is used as default but a specific version can be build from sources if new features are required.
##
## Compile and install custom opencv version instead of JetPack one (flags differ from default)
## Subscript is based on https://github.com/jetsonhacks/buildOpenCVTX2
##
## uncomment the two following lines to build opencv v3.4.2 from source and delete build files afterwards
# /bin/bash $HOME/neuroracer-robot-install/third_party/buildOpenCV_custom.sh
# rm -rf $HOME/opencv*

# install ros + build tools based on http://wiki.ros.org/melodic/Installation/Ubuntu:
ros_version="melodic"
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo -E apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
sudo apt update
sudo apt -y install ros-$ros_version-desktop-full

sudo apt -y install python-rosinstall python-rosinstall-generator python-wstool build-essential
# effort controllers (not included in dekstop-full)
sudo apt -y install ros-$ros_version-effort-controllers 
# tf2 sensor messages (not included in dekstop-full)
sudo apt -y install ros-$ros_version-tf2-sensor-msgs 
# ros-joy for controller support (not included in desktop-full)
sudo apt -y install ros-$ros_version-joy
# ros-rosserial-server for serialization support (not included in desktop-full)
sudo apt -y install ros-$ros_version-rosserial-server
# init ros and source 
sudo rosdep init
rosdep update

echo "# Provided by neuroracer-robot-install script" >> $HOME/.bashrc
echo "source /opt/ros/$ros_version/setup.bash" >> $HOME/.bashrc

# IMU visualization
sudo apt -y install python-visual

# PyPI
sudo apt -y install python-pip

# change to home folder to download wheels
cd $HOME

# Tensorflow - use pre compiled form source version, since ROS requires python 2.7 instead of the pre-build ones provided by nvidia (python 3.x only)
tensorflow_wheel="tensorflow-1.11.0-cp27-cp27mu-linux_aarch64.whl"
wget 'https://drive.google.com/uc?export=download&id=1GJEQEUF_ExvtSEMzmdsIGphB2Cq-A2D5' -O $tensorflow_wheel
pip install --user $tensorflow_wheel
rm $tensorflow_wheel

# Keras (since ubuntu 18.04 uses an older pip version ,we need a fortran compiler to be installed)
sudo apt -y install libatlas-base-dev gfortran
pip install --user keras-vis
# limit to 2.2.5 due to https://github.com/fizyr/keras-retinanet/issues/1117
pip uninstall --yes keras
pip install --user keras==2.2.5  

# PyTorch (https://devtalk.nvidia.com/default/topic/1049071/jetson-nano/pytorch-for-jetson-nano/)
torch_wheel="torch-1.2.0a0+8554416-cp27-cp27mu-linux_aarch64.whl"
wget https://nvidia.box.com/shared/static/8gcxrmcc6q4oc7xsoybk5wb26rkwugme.whl -O $torch_wheel
pip install --user $torch_wheel
rm $torch_wheel

# torchvision
sudo apt -y install libjpeg-dev zlib1g-dev
git clone -b v0.3.0 https://github.com/pytorch/vision torchvision
cd torchvision
python setup.py install --user
cd $HOME
rm -rf torchvision
