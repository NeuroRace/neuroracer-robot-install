#!/bin/bash

# fix cython issue on Jetson
pip install --user --upgrade cython

# source ROS to make sure its in PATH
source /opt/ros/melodic/setup.bash

# make NeuroRace structure and clone required repositories
mkdir $HOME/neurorace
cd $HOME/neurorace

# Common (Manual Control, etc.)
git clone https://gitlab.com/NeuroRace/neuroracer-common.git
pip install --user -e neuroracer-common

# AI
git clone https://gitlab.com/NeuroRace/neuroracer-ai.git
pip install --user -e neuroracer-ai

# Robot
git clone https://gitlab.com/NeuroRace/neuroracer-robot.git

# Config Server
cd neuroracer-robot/src
git clone https://gitlab.com/NeuroRace/neuroracer-configserver.git
cd neuroracer-configserver
python setup.py install --user

# Build Catkin Workspace
cd $HOME/neurorace/neuroracer-robot
catkin_make

# add sourcing of neuroracer ROS project to .bashrc if does not exist
grep -q -F 'source ~/neurorace/neuroracer-robot/devel/setup.bash' $HOME/.bashrc || echo "source ~/neurorace/neuroracer-robot/devel/setup.bash" >> $HOME/.bashrc

# add alias for launch to .bashrc if does not exist
grep -q -F "alias neuroracer_start='roslaunch neuroracer neuroracer.launch'" $HOME/.bashrc || echo "alias neuroracer_start='roslaunch neuroracer neuroracer.launch'" >> $HOME/.bashrc

