#!/bin/bash

# MODIFIED VERSION for NVIDIA SDKA 4.2 - Jetson TX2 r32.2.1
# ORIGIN: https://github.com/jetsonhacks/buildJetsonTX2Kernel/blob/master/scripts/makeKernel.sh
#
# MIT License
#
# Copyright (c) 2017-18 Jetsonhacks
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Builds the kernel and modules
# Assumes that the .config file is available in /proc/config.gz
# Added check to see if make builds correctly; retry once if not

cd /usr/src/kernel/kernel-4.9
make prepare
make modules_prepare
# Make alone will build the dts files too
# Get the number of CPUs 
NUM_CPU=$(nproc)
time make -j$(($NUM_CPU - 1)) Image
if [ $? -eq 0 ] ; then
  echo "Image make successful"
else
  # Try to make again; Sometimes there are issues with the build
  # because of lack of resources or concurrency issues
  echo "Make did not build " >&2
  echo "Retrying ... "
  # Single thread this time
  make Image
  if [ $? -eq 0 ] ; then
    echo "Image make successful"
  else
    # Try to make again
    echo "Make did not successfully build" >&2
    echo "Please fix issues and retry build"
    exit 1
  fi
fi

time make -j$(($NUM_CPU - 1)) modules
if [ $? -eq 0 ] ; then
  echo "Modules make successful"
else
  # Try to make again; Sometimes there are issues with the build
  # because of lack of resources or concurrency issues
  echo "Make did not build " >&2
  echo "Retrying ... "
  # Single thread this time
  make modules
  if [ $? -eq 0 ] ; then
    echo "Module make successful"
  else
    # Try to make again
    echo "Make did not successfully build" >&2
    echo "Please fix issues and retry build"
    exit 1
  fi
fi

make modules_install

