#!/bin/bash
# MIT License
#
# Copyright (c) 2017 Jetsonhacks
#
# Modified 2018 by Patrick Baumann, HTW Berlin
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

sudo apt install -y \
    cmake \
    libavcodec-dev \
    libavformat-dev \
    libavutil-dev \
    libeigen3-dev \
    libglew-dev \
    libgtk2.0-dev \
    libgtk-3-dev \
    libjasper-dev \
    libjpeg-dev \
    libpng12-dev \
    libpostproc-dev \
    libswscale-dev \
    libtbb-dev \
    libtiff5-dev \
    libv4l-dev \
    libxvidcore-dev \
    libx264-dev \
    qt5-default \
    zlib1g-dev \
    checkinstall \
    pkg-config

# Python 2.7
sudo apt install -y python-dev python-numpy python-py python-pytest
# GStreamer support
sudo apt install -y libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly

# opencv
cd $HOME/
git clone https://github.com/opencv/opencv.git
git checkout -b v3.4.2 3.4.2
mkdir opencv/build
cd opencv/build

# Jetson TX2
cmake \
    -D CMAKE_BUILD_TYPE=Release \
    -D CMAKE_INSTALL_PREFIX=/usr \
    -D BUILD_EXAMPLES=OFF \
    -D BUILD_opencv_java=OFF \
    -D BUILD_opencv_python2=ON \
    -D BUILD_opencv_python3=OFF \
    -D ENABLE_PRECOMPILED_HEADERS=OFF \
    -D WITH_OPENCL=OFF \
    -D WITH_OPENGL=OFF \
    -D WITH_OPENMP=OFF \
    -D WITH_LIBV4L=ON \
    -D WITH_FFMPEG=ON \
    -D WITH_GSTREAMER=ON \
    -D WITH_GSTREAMER_0_10=OFF \
    -D WITH_CUDA=ON \
    -D WITH_QT=ON \
    -D CUDA_TOOLKIT_ROOT_DIR=/usr/local/ \
    -D CUDA_ARCH_BIN=6.2 \
    -D CUDA_ARCH_PTX="" \
    -D CUDA_FAST_MATH=ON \
    -D ENABLE_FAST_MATH=ON \
    -D WITH_CUBLAS=ON \
    -D INSTALL_C_EXAMPLES=OFF \
    -D INSTALL_TESTS=OFF \
    ../

# build
make -j6

# The build system has been known at times to have issues. It's worth doing a sanity check after the build
make

# install the new build
sudo checkinstall \
    --pkgname='opencv-nr' \
    --pkgversion='3.4.2' \
    --pkgrelease='1' \
    --pkglicense='3-clause BSD License' \
    --nodoc \
    --maintainer='Patrick Baumann' \
    --pkgarch=$(dpkg --print-architecture) \
    --pkggroup='NeuroRace' \
    --pkgsource='https://gitlab.com/NeuroRace/neuroracer-install/blob/master/external/buildOpenCV_custom.sh' \
    --provides='opencv-nr' \
    --requires='gstreamer1.0-plugins-bad, gstreamer1.0-plugins-ugly' \
    --conflicts='opencv' \
    --default

