#!/bin/bash
function cleanup {
	sudo rm /usr/src/public_sources.tbz2
	sudo rm -r /usr/src/hardware
	sudo rm -r /usr/src/kernel
	sudo rm -r /usr/src/public_sources
	sudo rm -r $HOME/WireGuard
}

if [ "$1" == "clean" ]
then
	cleanup
	exit 0
fi

# get git directory path
git_path="`dirname \"$0\"`"              # relative
git_path="`( cd \"$git_path\" && pwd )`"  # absolutized and normalized
if [ -z "$git_path" ] ; then
  # error; for some reason, the path is not accessible
  # to the script (e.g. permissions re-evaled after suid)
  exit 1  # fail
fi

# install module dependencies
sudo apt -y install arduino arduino-core

# install kernel built dependencies
sudo apt -y install qt5-default pkg-config

# get the kernel sources from the NVIDIA developer website, then unpack the sources into /usr/src/kernel/kernel-4.9/
cd /usr/src
sudo wget -N https://developer.nvidia.com/embedded/dlc/r32-2-1_Release_v1.0/TX2-AGX/sources/public_sources.tbz2
sudo tar -xvf public_sources.tbz2 public_sources/kernel_src.tbz2
sudo tar -xvf public_sources/kernel_src.tbz2
cd kernel/kernel-4.9
sudo bash -c 'zcat /proc/config.gz > .config'

# set usb acm
sudo sed -i 's/.*CONFIG_USB_ACM.*/CONFIG_USB_ACM=y/' /usr/src/kernel/kernel-4.9/.config
# set xpad rumble
sudo sed -i 's/.*# CONFIG_JOYSTICK_XPAD_FF is not set.*/CONFIG_JOYSTICK_XPAD_FF=y/' /usr/src/kernel/kernel-4.9/.config
# set xpad led
sudo sed -i 's/.*# CONFIG_JOYSTICK_XPAD_LEDS is not set.*/CONFIG_JOYSTICK_XPAD_LEDS=y/' /usr/src/kernel/kernel-4.9/.config
# Tegra-Throughput
sudo bash -c 'echo "CONFIG_TEGRA_THROUGHPUT=y" >> /usr/src/kernel/kernel-4.9/.config'

# some new params in 4.9 which need to be set otherwise user input is required
sudo bash -c 'echo "PRINTK_SAFE_LOG_BUF_SHIFT=13" >> /usr/src/kernel/kernel-4.9/.config'
sudo bash -c 'echo "THERMAL_GOV_CONTINUOUS=n" >> /usr/src/kernel/kernel-4.9/.config'
sudo bash -c 'echo "LEDS_TRIGGER_THROTTLE=n" >> /usr/src/kernel/kernel-4.9/.config'
sudo bash -c 'echo "TEGRA_CPU_TOPOLOGY_DEBUGFS=n" >> /usr/src/kernel/kernel-4.9/.config'
sudo bash -c 'echo "TEGRA_CPU_TOPOLOGY_SYSFS=m" >> /usr/src/kernel/kernel-4.9/.config'

# set wireguard
cd $HOME
git clone https://git.zx2c4.com/WireGuard
cd /usr/src/kernel/kernel-4.9/
sudo bash -c 'bash $HOME/WireGuard/contrib/kernel-tree/create-patch.sh | patch -p1'
sudo bash -c 'echo "CONFIG_WIREGUARD=m" >> /usr/src/kernel/kernel-4.9/.config'
sudo bash -c 'echo "CONFIG_WIREGUARD_DEBUG=n" >> /usr/src/kernel/kernel-4.9/.config'
sudo bash -c 'echo "CONFIG_GENEVE=n" >> /usr/src/kernel/kernel-4.9/.config'

# put device in power mode
sudo nvpmodel -m 0

# build the kernel and modules specified
cd $git_path/third_party/jetsonhacks/
sudo bash $git_path/third_party/jetsonhacks/makeKernel.sh

# copy over the newly built Image and zImage files into the /boot directory
sudo bash $git_path/third_party/jetsonhacks/copyImage.sh

# cleanup
cleanup

