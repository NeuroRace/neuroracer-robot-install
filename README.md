# NeuroRacer Installer
To install the NeuroRace project on a Jetson TX2 follow the prerequisites and execute the [installNeuroRacer shell script](installNeuroRacer.sh). The optional script to build OpenCV from source is based on the official repositories from JetsonHacks but has been customized with different compiling flags and automized to not require any user input.

## On this page
- [Prerequisites](#prerequisites)
- [Installation](#installation)
  - [OpenCV from Source](#opencv-from-source)
  - [Performance Tuning](#performance-tuning)
- [Additional Hardware](#additional-hardware)

## Prerequisites 

* Flash Jetson

Use the [NVIDIA SDK Manager](https://developer.nvidia.com/nvidia-sdk-manager) and choose your Jetson device (for example TX2). Afterwards select all components to be installed as seen in the following figure.  

| ![packages](docs/tx2_flash_packages.png) |
|:--:|
| _NVIDIA SDK Manager ([https://developer.nvidia.com/nvidia-sdk-manager](https://developer.nvidia.com/nvidia-sdk-manager))_ |

<br>

* Remove sudo's timeout

The install script will run for some time, requiring sudo to install and build packages as well as creating directories and files. To prevent required user interaction while installing, deactivate the timeout.

```
edit /etc/sudoers
```

Add:

```
Defaults        timestamp_timeout=-1
```

* Update system and clean cache

```
sudo apt update  
sudo apt upgrade  
sudo apt autoremove  
sudo apt autoclean  
sudo apt clean
```

* Reboot system

## Installation
* Clone `neuroracer-robot-install` repository to user's home directory

```
git clone https://gitlab.com/NeuroRace/neuroracer-robot-install.git
```
* Execute [`installNeuroRacer.sh`](installNeuroRacer.sh) (installs required packages and sets up NeuroRace project)
* Execute [`buildCustomKernelTX2.sh`](buildCustomKernelTX2.sh) (required for acm, wireguard and xpad)
* Reboot system

__Note:__  
Pay attention when updating your Jetson Ubuntu Software to not override customized packages (`opencv` - if built from source, `linux-kernel` - if custom one is used).

#### OpenCV from Source
Since the project makes use of the ZED camera, the onboard OmniVision is not used and the provided OpenCV from JetPack can be used. But to build OpenCV from source and make use of the OmniVision Camera for example, purge all opencv packages (or don't flash OpenCV from Jetpack) and run [third_party/buildOpenCV_custom.sh](third_party/buildOpenCV_custom.sh). The script uses custom flags and requires no user interaction.
* Reference: [https://github.com/jetsonhacks/buildOpenCVTX2](https://github.com/jetsonhacks/buildOpenCVTX2)

#### Performance Tuning
* To remove bloat services wasting resources, i.e. GUI, execute the according script
  * [disable services](scripts/disable_services.sh)
  * reboot
* To reactivate
  * [enable services](scripts/enable_services.sh)
  * reboot

__Note:__  
The scripts should run via console/ssh due to GUI de-/activating services.

## Additional Hardware
For information how to install additional supported hardware for the robot, see [neuroracer-robot](https://gitlab.com/NeuroRace/neuroracer-robot/tree/master#add-ons-sources-not-included). 
