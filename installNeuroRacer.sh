#!/bin/bash
# get git directory path
git_path="`dirname \"$0\"`"              # relative
git_path="`( cd \"$git_path\" && pwd )`"  # absolutized and normalized
if [ -z "$git_path" ] ; then
  # error; for some reason, the path is not accessible
  # to the script (e.g. permissions re-evaled after suid)
  exit 1  # fail
fi

# optimize system before installation
/bin/bash $git_path/scripts/optimizeTegraUbuntu.sh

# install required components
/bin/bash $git_path/scripts/setupPrerequisites.sh

# setup NeuroRace project
/bin/bash $git_path/scripts/setupProject.sh

